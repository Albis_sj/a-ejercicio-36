import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ruta-err',
  templateUrl: './pagina-no-encontrada.component.html',
  styleUrls: ['./pagina-no-encontrada.component.css']
})
export class PaginaNoEncontradaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
